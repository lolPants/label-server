Label Server
Generates Name Labels Dynamically
Because why the hell not?

GET /api/v1.0/generate/:name
:name [Name Label Text] (Limit of 300 characters)
STATUS 200 [PNG]
STATUS 500 [Server Error]

GET /api/v1.0/raw/:name
Same as /generate but doesn't split dynamically based on length
Also lowers the limit to 50 characters
