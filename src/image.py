from PIL import Image, ImageFont, ImageDraw

def generate_label(text):
  # Fill up all available space
  size_check = 10
  target_size = (1960, 760)

  # Check for multiline strings
  if '\n' in text:
    test_text = max(text.split('\n'), key=len)
  else:
    test_text = text

  while True:
    test_font = ImageFont.truetype(font='./comic.ttf', size=size_check)
    font_size = test_font.getsize(test_text)
    if font_size[0] > target_size[0] or font_size[1] > target_size[1]:
      break
    else:
      font = test_font
      size_check += 10

  # Open Image
  hello = Image.open('label.png').convert('RGBA')

  # Center Everything
  size = font.getsize(test_text)

  # Calculate Size Correctly (handles multiline strings)
  size_x = size[0]
  if '\n' in text:
    size_y = size[1] * len(text.split('\n'))
  else:
    size_y = size[1]

  # Calculate X,Y for the text
  y_offset = 520
  x = ((target_size[0] - size_x) / 2) + ((hello.size[0] - target_size[0]) / 2)
  y = y_offset + (target_size[1] / 2) - (size_y / 2)

  # Draw the Text
  d = ImageDraw.Draw(hello)
  d.text((x, y), text, font=font, fill=(0, 0, 0, 255))
  del d

  # Output
  return hello
