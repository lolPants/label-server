import io
import re
from urllib.parse import unquote
from image import generate_label
from flask import Flask, send_file, abort

app = Flask(__name__)

@app.route('/')
def index():
  return send_file('index.txt')

@app.route('/api/v1.0/generate/<raw>')
def serve(raw):
  label = unquote(raw)
  label = re.sub(r'.{30}\S*\s+', r'\g<0>@-@', label).replace(' @-@', '\n')

  if len(label) > 100:
    abort(400)
  else:
    image = generate_label(label)
    byte_io = io.BytesIO()
    image.save(byte_io, 'PNG')
    byte_io.seek(0)

    return send_file(byte_io, mimetype='image/png')

@app.route('/api/v1.0/raw/<raw>')
def serveraw(raw):
  label = unquote(raw)

  if len(label) > 50:
    abort(400)
  else:
    image = generate_label(label)
    byte_io = io.BytesIO()
    image.save(byte_io, 'PNG')
    byte_io.seek(0)

    return send_file(byte_io, mimetype='image/png')

if __name__ == '__main__':
  app.run(host='0.0.0.0', port=8080)
